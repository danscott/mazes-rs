#![allow(dead_code)]
use tcod::console::{FontLayout, Root};
use tcod::input::{self, Event, Key};

use std::cell::RefCell;
use std::rc::Rc;

mod map;
mod map_generator;
mod maze_generator;
mod render;

const SCR_WIDTH: i32 = 100;
const SCR_HEIGHT: i32 = 100;
const LIMIT_FPS: i32 = 20;

const MAIN_WIDTH: i32 = (SCR_WIDTH - 1) / 2;
const MAIN_HEIGHT: i32 = (SCR_HEIGHT - 1) / 2;

const FONT: &str = "courier8x8_aa_tc.png";

pub use map::*;

use maze_generator::*;
use render::*;

trait Generator {
    fn build_step(&mut self) -> GenResult;
}

#[derive(Debug)]
enum GenResult {
    Partial(Map),
    Done(Map),
}

fn main() {
    let inner_root = Root::initializer()
        .title("mazes")
        .size(SCR_WIDTH, SCR_HEIGHT)
        .font(FONT, FontLayout::Tcod)
        .init();

    let root = Rc::new(RefCell::new(inner_root));

    let mut renderer = TcodRenderer::new(&root);

    tcod::system::set_fps(LIMIT_FPS);

    let mut generator = map_generator::CellularGen::new(SCR_WIDTH, SCR_HEIGHT);

    let mut map: Option<Map> = None;

    let mut generated = false;

    while !root.borrow().window_closed() {
        //        let key = match input::check_for_event(input::KEY_PRESS) {
        //            Some((_, Event::Key(k))) => Some(k),
        //            _ => None,
        //        };

        let key = root.borrow_mut().wait_for_keypress(true);
        match key {
            Key {
                code: tcod::input::KeyCode::Escape,
                ..
            } => break,
            _ => {}
        }

        if !generated {
            let next_step = generator.build_step();

            map = match next_step {
                GenResult::Done(m) => {
                    generated = true;
                    Some(m)
                }
                GenResult::Partial(m) => Some(m),
            };
        }

        match &map {
            Some(m) => renderer.render(&m),
            _ => {}
        }
    }
}
