use crate::{Coordinate, Map, Tile};
use std::cell::RefCell;
use std::rc::Rc;
use tcod::colors;
use tcod::console::{BackgroundFlag, Console, Root};

pub trait MapRenderer {
    fn render(&mut self, map: &Map);
}

pub struct TcodRenderer<'a> {
    root: &'a Rc<RefCell<Root>>,
}

impl<'a> TcodRenderer<'a> {
    pub fn new(root: &Rc<RefCell<Root>>) -> TcodRenderer {
        TcodRenderer { root }
    }

    fn clear(&mut self) {
        let mut root = self.root.borrow_mut();
        root.set_default_background(colors::BLACK);
        root.clear();
    }

    fn flush(&mut self) {
        let mut root = self.root.borrow_mut();
        root.flush();
    }
}

impl<'a> MapRenderer for TcodRenderer<'a> {
    fn render(&mut self, map: &Map) {
        self.clear();
        for (Coordinate { x, y }, tile) in map {
            let color = match tile {
                Tile::Wall => colors::BLACK,
                Tile::Wood => colors::BRASS,
                Tile::Lava => colors::RED,
                Tile::Water => colors::LIGHT_BLUE,
                Tile::Stone => colors::DARK_GREY,
            };

            let mut root = self.root.borrow_mut();

            root.set_default_background(color);
            root.set_default_foreground(color);

            root.put_char(x, y, ' ', BackgroundFlag::Set);
        }
        self.flush();
    }
}
