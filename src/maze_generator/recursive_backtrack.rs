use super::{get_opposite_wall, get_wall, DirGen, MazeGenerator, MazeGeneratorResult};
use crate::{Coordinate, Maze};
use rand::prelude::*;

pub struct RecursiveBacktrack {
    maze: Maze,
    start: Coordinate,
    current: Coordinate,
    rng: ThreadRng,
    dirs: DirGen,
    backtracking: bool,
    done: bool,
}

impl RecursiveBacktrack {
    pub fn new(width: i32, height: i32) -> RecursiveBacktrack {
        let mut maze = Maze::new(width, height);

        let mut rng = thread_rng();

        let start = Coordinate::new(rng.gen_range(0, width), rng.gen_range(0, height));

        let current = start.clone();

        maze.visit_tile(&current);

        RecursiveBacktrack {
            maze,
            start,
            current,
            rng,
            dirs: DirGen::new(),
            backtracking: false,
            done: false,
        }
    }

    fn is_in_bounds(&self, pos: &Coordinate) -> bool {
        pos.x >= 0 && pos.y >= 0 && pos.x < self.maze.dim.width && pos.y < self.maze.dim.height
    }
}

impl MazeGenerator for RecursiveBacktrack {
    fn build_step(&mut self) -> MazeGeneratorResult {
        if self.done {
            return MazeGeneratorResult::Done(self.maze.clone());
        }

        if self.backtracking {
            self.maze.lock_tile(&self.current);

            if self.current == self.start {
                self.done = true;
                println!("Done!");
            } else {
                let tile = self.maze.get_tile(&self.current).unwrap();

                'try_backtrack: for dir in &self.dirs.dirs {
                    if !tile.walls.contains(get_wall(dir)) {
                        let next = self.current + self.dirs.delta(dir);
                        if !self.maze.is_locked(&next) {
                            self.current = next;
                            break 'try_backtrack;
                        }
                    }
                }

                if self
                    .dirs
                    .dirs
                    .iter()
                    .any(|d| !self.maze.is_visited(&(self.current + self.dirs.delta(d))))
                {
                    self.backtracking = false;
                }
            }
        } else {
            let mut moved = false;
            self.dirs.dirs.shuffle(&mut self.rng);

            'try_advance: for dir in &self.dirs.dirs {
                let next = self.current + self.dirs.delta(dir);

                if self.is_in_bounds(&next) && !self.maze.is_visited(&next) {
                    self.maze.visit_tile(&next);

                    self.maze.remove_wall(&self.current, get_wall(dir));

                    self.maze.remove_wall(&next, get_opposite_wall(dir));

                    self.current = next;
                    moved = true;
                    break 'try_advance;
                }
            }

            if !moved {
                self.backtracking = true;
            }
        }

        MazeGeneratorResult::Partial(self.maze.clone())
    }
}
