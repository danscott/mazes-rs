use super::{MazeGenerator, MazeGeneratorResult};
use crate::{Coordinate, Maze, Wall};

use rand::prelude::*;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Direction {
	North,
	South,
	East,
	West,
}

fn get_wall(dir: &Direction) -> Wall {
	match dir {
		Direction::North => Wall::NORTH,
		Direction::South => Wall::SOUTH,
		Direction::East => Wall::EAST,
		Direction::West => Wall::WEST,
	}
}

fn get_opposite_wall(dir: &Direction) -> Wall {
	match dir {
		Direction::North => Wall::SOUTH,
		Direction::South => Wall::NORTH,
		Direction::East => Wall::WEST,
		Direction::West => Wall::EAST,
	}
}

pub struct Wilsons {
	maze: Maze,
	maze_inprog: Maze,
	walking: bool,
	free: HashSet<Coordinate>,
	visits: HashMap<Coordinate, Direction>,
	dirs: [Direction; 4],
	dir_delta: HashMap<Direction, Coordinate>,
	rng: ThreadRng,
	start: Coordinate,
	current: Coordinate,
}

impl Wilsons {
	pub fn new(width: i32, height: i32) -> Wilsons {
		let maze = Maze::new(width, height);
		let maze_inprog = maze.clone();
		let visits = HashMap::new();
		let mut free = HashSet::new();

		for x in 0..width {
			for y in 0..height {
				free.insert(Coordinate::new(x, y));
			}
		}

		let dirs = [
			Direction::North,
			Direction::South,
			Direction::East,
			Direction::West,
		];

		let mut dir_delta: HashMap<Direction, Coordinate> = HashMap::new();
		dir_delta.insert(Direction::North, Coordinate::new(0, -1));
		dir_delta.insert(Direction::South, Coordinate::new(0, 1));
		dir_delta.insert(Direction::East, Coordinate::new(1, 0));
		dir_delta.insert(Direction::West, Coordinate::new(-1, 0));

		let mut gen = Wilsons {
			maze,
			maze_inprog,
			walking: true,
			free,
			visits,
			dirs,
			dir_delta,
			rng: thread_rng(),
			start: Coordinate::new(0, 1),
			current: Coordinate::new(0, 1),
		};

		let initial = gen.next_free_coord().unwrap_or(Coordinate::new(0, 0));

		gen.maze.lock_tile(&initial);
		gen.maze_inprog.lock_tile(&initial);
		gen.free.remove(&initial);

		gen.start = gen.next_free_coord().unwrap_or(Coordinate::new(0, 1));
		gen.current = gen.start.clone();

		gen
	}

	fn next_free_coord(&mut self) -> Option<Coordinate> {
		(&self.free)
			.into_iter()
			.choose(&mut self.rng)
			.map(|result| result.clone())
	}

	fn get_neighbour(&self, dir: &Direction) -> Coordinate {
		self.current + *self.dir_delta.get(&dir).unwrap()
	}

	fn is_in_bounds(&self, pos: &Coordinate) -> bool {
		pos.x >= 0 && pos.y >= 0 && pos.x < self.maze.dim.width && pos.y < self.maze.dim.height
	}

}

impl MazeGenerator for Wilsons {
	fn build_step(&mut self) -> MazeGeneratorResult {
		if !self.walking {
			return MazeGeneratorResult::Done(self.maze.clone());
		}

		self.dirs.shuffle(&mut self.rng);

		'dirloop: for dir in &self.dirs {
			let next_coord = self.get_neighbour(dir);
			if self.is_in_bounds(&next_coord) {
				self.visits.insert(self.current, *dir);

				self.maze_inprog.visit_tile(&next_coord);

				self
					.maze_inprog
					.remove_wall(&self.current, get_wall(dir));

				self
					.maze_inprog
					.remove_wall(&next_coord, get_opposite_wall(dir));

				if self.maze.is_locked(&next_coord) {
					self.current = self.start;

					'copyloop: loop {
						match self.visits.get(&self.current) {
							Some(dir) => {
								self.maze.lock_tile(&self.current);

								self.free.remove(&self.current);

								self
									.maze
									.remove_wall(&self.current, get_wall(dir));

								self.current = self.get_neighbour(dir);

								self
									.maze
									.remove_wall(&self.current, get_opposite_wall(dir));
							}
							_ => break 'copyloop,
						}
					}

					self.visits.clear();
					self.maze_inprog = self.maze.clone();

					match &self.next_free_coord() {
						Some(next_coord) => {
							self.start = next_coord.clone();
							self.current = next_coord.clone();
						}
						None => {
							self.walking = false;
						}
					}
				} else {
					self.current = next_coord;
				}
				break 'dirloop;
			}
		}

		MazeGeneratorResult::Partial(self.maze_inprog.clone())
	}
}