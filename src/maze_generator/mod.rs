use std::collections::HashMap;

mod recursive_backtrack;
mod wilsons;

use bitflags::*;

use crate::{Coordinate, Dimensions, Map, Tile};
pub use recursive_backtrack::*;
pub use wilsons::*;

pub enum MazeGeneratorResult {
    Partial(Maze),
    Done(Maze),
}

pub trait MazeGenerator {
    fn build_step(&mut self) -> MazeGeneratorResult;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Direction {
    North,
    South,
    East,
    West,
}

struct DirGen {
    dirs: [Direction; 4],
    dir_delta: HashMap<Direction, Coordinate>,
}

impl DirGen {
    fn new() -> DirGen {
        let dirs = [
            Direction::North,
            Direction::South,
            Direction::East,
            Direction::West,
        ];

        let mut dir_delta: HashMap<Direction, Coordinate> = HashMap::new();
        dir_delta.insert(Direction::North, Coordinate::new(0, -1));
        dir_delta.insert(Direction::South, Coordinate::new(0, 1));
        dir_delta.insert(Direction::East, Coordinate::new(1, 0));
        dir_delta.insert(Direction::West, Coordinate::new(-1, 0));

        DirGen { dirs, dir_delta }
    }

    fn delta(&self, dir: &Direction) -> Coordinate {
        match self.dir_delta.get(dir) {
            Some(c) => c.clone(),
            _ => Coordinate::new(0, 0),
        }
    }
}

fn get_wall(dir: &Direction) -> Wall {
    match dir {
        Direction::North => Wall::NORTH,
        Direction::South => Wall::SOUTH,
        Direction::East => Wall::EAST,
        Direction::West => Wall::WEST,
    }
}

fn get_opposite_wall(dir: &Direction) -> Wall {
    match dir {
        Direction::North => Wall::SOUTH,
        Direction::South => Wall::NORTH,
        Direction::East => Wall::WEST,
        Direction::West => Wall::EAST,
    }
}

bitflags! {
    pub struct Wall: u32 {
        const NONE = 0b0000;
        const NORTH = 0b0001;
        const SOUTH = 0b0010;
        const EAST = 0b0100;
        const WEST = 0b1000;
        const NS = Self::NORTH.bits | Self::SOUTH.bits;
        const EW = Self::EAST.bits | Self::WEST.bits;
        const NE = Self::NORTH.bits | Self::EAST.bits;
        const NW = Self::NORTH.bits | Self::WEST.bits;
        const SE = Self::SOUTH.bits | Self::EAST.bits;
        const SW = Self::SOUTH.bits | Self::WEST.bits;
        const TN = Self::EW.bits | Self::NORTH.bits;
        const TS = Self::EW.bits | Self::SOUTH.bits;
        const TE = Self::NS.bits | Self::EAST.bits;
        const TW = Self::NS.bits | Self::WEST.bits;
        const ALL = Self::NS.bits | Self::EW.bits;
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum MazeTileState {
    Unvisited,
    Locked,
    Visited,
}

#[derive(Clone, Copy)]
pub struct MazeTile {
    pub walls: Wall,
    pub state: MazeTileState,
}

impl MazeTile {
    pub fn new(walls: Wall) -> MazeTile {
        MazeTile {
            walls,
            state: MazeTileState::Unvisited,
        }
    }
}

#[derive(Clone)]
pub struct Maze {
    pub dim: Dimensions,
    tiles: HashMap<Coordinate, MazeTile>,
}

impl Maze {
    pub fn new(width: i32, height: i32) -> Maze {
        let mut maze = Maze {
            dim: Dimensions { width, height },
            tiles: HashMap::new(),
        };

        for x in 0..width {
            for y in 0..height {
                maze.tiles
                    .insert(Coordinate::new(x, y), MazeTile::new(Wall::ALL));
            }
        }

        maze
    }

    pub fn visit_tile(&mut self, coord: &Coordinate) {
        match self.tiles.get_mut(coord) {
            Some(tile) => tile.state = MazeTileState::Visited,
            _ => {}
        }
    }

    pub fn lock_tile(&mut self, coord: &Coordinate) {
        match self.tiles.get_mut(coord) {
            Some(tile) => tile.state = MazeTileState::Locked,
            _ => {}
        }
    }

    pub fn is_locked(&self, coord: &Coordinate) -> bool {
        match self.tiles.get(coord) {
            Some(tile) => tile.state == MazeTileState::Locked,
            _ => false,
        }
    }

    pub fn is_visited(&self, coord: &Coordinate) -> bool {
        match self.tiles.get(coord) {
            Some(tile) => tile.state != MazeTileState::Unvisited,
            _ => false,
        }
    }

    pub fn get_tile(&self, coord: &Coordinate) -> Option<&MazeTile> {
        self.tiles.get(coord)
    }

    pub fn remove_wall(&mut self, coord: &Coordinate, wall: Wall) {
        match self.tiles.get_mut(coord) {
            Some(t) => {
                t.walls = t.walls - wall;
            }
            None => {}
        }
    }
}

impl From<&Maze> for Map {
    fn from(maze: &Maze) -> Self {
        let north: Coordinate = Coordinate::new(0, -1);
        let south: Coordinate = Coordinate::new(0, 1);
        let west: Coordinate = Coordinate::new(-1, 0);
        let east: Coordinate = Coordinate::new(1, 0);
        let maze_dim = Dimensions {
            width: maze.dim.width * 2 + 2,
            height: maze.dim.height * 2 + 2,
        };
        let mut map = Map::new(maze_dim);
        for (Coordinate { x, y }, maze_tile) in &maze.tiles {
            let map_coord = Coordinate::new(x * 2 + 1, y * 2 + 1);
            let tile = match maze_tile.state {
                MazeTileState::Unvisited => Tile::Wall,
                MazeTileState::Locked => Tile::Stone,
                MazeTileState::Visited => Tile::Water,
            };
            map.set_tile(&map_coord, tile);

            if maze_tile.state == MazeTileState::Unvisited {
                continue;
            }

            if !maze_tile.walls.contains(Wall::NORTH) {
                map.set_tile(&(map_coord + north), tile)
            }

            if !maze_tile.walls.contains(Wall::SOUTH) {
                map.set_tile(&(map_coord + south), tile);
            }

            if !maze_tile.walls.contains(Wall::EAST) {
                map.set_tile(&(map_coord + east), tile);
            }

            if !maze_tile.walls.contains(Wall::WEST) {
                map.set_tile(&(map_coord + west), tile);
            }
        }

        map
    }
}
