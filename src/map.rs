use std::ops::Add;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Tile {
    Wall,
    Wood,
    Lava,
    Water,
    Stone,
}

#[derive(Debug, Clone, Copy)]
pub struct Dimensions {
    pub width: i32,
    pub height: i32,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Coordinate {
    pub x: i32,
    pub y: i32,
}

impl Coordinate {
    pub fn new(x: i32, y: i32) -> Coordinate {
        Coordinate { x, y }
    }

    pub fn scan_next(&mut self, dim: &Dimensions) {
        self.x += 1;
        if self.x >= dim.width {
            self.x = 0;
            self.y += 1;
            if self.y >= dim.height {
                self.y = 0;
            }
        }
    }

    pub fn is_origin(&self) -> bool {
        self.x == 0 && self.y == 0
    }

    pub fn neighbours(&self, dim: &Dimensions) -> Vec<Coordinate> {
        (&[
            Coordinate::new(self.x - 1, self.y - 1),
            Coordinate::new(self.x - 1, self.y),
            Coordinate::new(self.x - 1, self.y + 1),
            Coordinate::new(self.x, self.y - 1),
            Coordinate::new(self.x, self.y + 1),
            Coordinate::new(self.x + 1, self.y - 1),
            Coordinate::new(self.x + 1, self.y),
            Coordinate::new(self.x + 1, self.y + 1),
        ])
            .iter()
            .filter(|&c| c.x >= 0 && c.x < dim.width && c.y >= 0 && c.y < dim.height)
            .map(|&c| c)
            .collect()
    }
}

impl Add for Coordinate {
    type Output = Coordinate;

    fn add(self, other: Coordinate) -> Coordinate {
        Coordinate {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Debug)]
pub struct Map {
    dim: Dimensions,
    tiles: Vec<Tile>,
}

impl Map {
    pub fn new(dim: Dimensions) -> Self {
        let tiles = vec![Tile::Wall; dim.width as usize * dim.height as usize];

        Self { dim, tiles }
    }

    pub fn set_tile(&mut self, coord: &Coordinate, tile: Tile) {
        let idx = self.get_coord_idx(coord);
        self.tiles[idx] = tile;
    }

    fn get_coord_idx(&self, coord: &Coordinate) -> usize {
        get_coord_idx(coord, self.dim.width)
    }
}

fn get_coord_idx(coord: &Coordinate, width: i32) -> usize {
    coord.x as usize + (coord.y * width) as usize
}

impl<'a> IntoIterator for &'a Map {
    type Item = (Coordinate, Tile);
    type IntoIter = MapIntoIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        MapIntoIterator {
            map: &self.tiles,
            dim: self.dim,
            current_coord: Coordinate::new(0, 0),
            ct: 0,
        }
    }
}

pub struct MapIntoIterator<'a> {
    map: &'a Vec<Tile>,
    dim: Dimensions,
    ct: usize,
    current_coord: Coordinate,
}

impl<'a> Iterator for MapIntoIterator<'a> {
    type Item = (Coordinate, Tile);

    fn next(&mut self) -> Option<Self::Item> {
        if self.ct >= self.map.len() {
            return None;
        }

        self.ct += 1;

        let idx = get_coord_idx(&self.current_coord, self.dim.width);

        let next_tile = Some((self.current_coord, self.map[idx as usize]));

        self.current_coord.scan_next(&self.dim);

        next_tile
    }
}
