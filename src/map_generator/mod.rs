use std::vec::Vec;

use crate::{Coordinate, Dimensions, GenResult, Generator, Map, Tile};
use rand::prelude::ThreadRng;
use rand::{thread_rng, Rng};

#[derive(Debug, Clone)]
pub struct CellMap {
    dim: Dimensions,
    tiles: Vec<Tile>,
}

impl CellMap {
    pub fn new(width: i32, height: i32) -> CellMap {
        let dim = Dimensions { width, height };
        let tiles = vec![Tile::Wall; width as usize * height as usize];

        CellMap { dim, tiles }
    }

    pub fn get_tile(&self, coord: &Coordinate) -> Tile {
        self.tiles[(coord.x + coord.y * self.dim.width) as usize]
    }

    pub fn set_tile(&mut self, coord: &Coordinate, tile: Tile) {
        self.tiles[(coord.x + coord.y * self.dim.width) as usize] = tile;
    }
}

pub struct CellularGen {
    map: CellMap,
    next_map: CellMap,
    current_coord: Coordinate,
    gen_count: i32,
    rng: ThreadRng,
}

impl CellularGen {
    pub fn new(width: i32, height: i32) -> Self {
        let map = CellMap::new(width, height);
        let next_map = CellMap::new(width, height);
        let gen_count = 0;
        let current_coord = Coordinate::new(0, 0);

        CellularGen {
            map,
            next_map,
            current_coord,
            gen_count,
            rng: thread_rng(),
        }
    }

    fn set_current_tile(&mut self, tile: Tile) {
        self.next_map.set_tile(&self.current_coord, tile);
    }

    fn advance(&mut self) {
        self.current_coord.scan_next(&self.map.dim);
        if self.current_coord.is_origin() {
            for idx in 0..self.map.tiles.len() {
                self.map.tiles[idx] = self.next_map.tiles[idx];
            }
            self.gen_count += 1;
        }
    }

    fn wall_count(&mut self) -> usize {
        self.current_coord
            .neighbours(&self.map.dim)
            .iter()
            .filter(|c| self.map.get_tile(c) == Tile::Wall)
            .count()
    }

    fn is_edge(&self) -> bool {
        self.current_coord.x == 0
            || self.current_coord.x == self.map.dim.width - 1
            || self.current_coord.y == 0
            || self.current_coord.y == self.map.dim.height - 1
    }

    fn get_tile(&self) -> Tile {
        self.map.get_tile(&self.current_coord)
    }

    fn check_wall(&mut self) {
        if self.is_edge() {
            self.set_current_tile(Tile::Wall);
            return;
        }
        let tile = self.get_tile();
        let wall_count = self.wall_count();

        let next_tile = if (tile == Tile::Wall && wall_count > 4) || wall_count > 5 {
            Tile::Wall
        } else {
            Tile::Stone
        };

        self.set_current_tile(next_tile)
    }
}

impl Generator for CellularGen {
    fn build_step(&mut self) -> GenResult {
        if self.gen_count == 0 {
            while self.gen_count == 0 {
                if self.is_edge() {
                    self.set_current_tile(Tile::Wall)
                } else {
                    let tile = match self.rng.gen::<f32>() < 0.38 {
                        false => Tile::Wall,
                        true => Tile::Stone,
                    };
                    self.set_current_tile(tile);
                }
                self.advance();
            }
        } else {
            let gt = self.gen_count;
            while self.gen_count == gt {
                self.check_wall();
                self.advance()
            }
        }

        let map = (&self.next_map).into();

        GenResult::Partial(map)
    }
}

impl From<&CellMap> for Map {
    fn from(cell_map: &CellMap) -> Map {
        let mut map = Map::new(cell_map.dim);
        for x in 0..cell_map.dim.width {
            for y in 0..cell_map.dim.height {
                let tile = match cell_map.get_tile(&Coordinate::new(x, y)) {
                    Tile::Wall => Tile::Wall,
                    _ => Tile::Stone,
                };
                map.set_tile(&Coordinate::new(x, y), tile);
            }
        }

        map
    }
}
